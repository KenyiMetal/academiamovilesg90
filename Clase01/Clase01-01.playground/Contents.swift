import UIKit

var name1 = "Kenyi"
var name2: String = "Kenyi"

//func nombreFuncion(descrip(opc.) variable: TipoDato) -> TipoDato

func sumar(numero1: Int, numero2: Int) -> Int {
    return numero1 + numero2
}

sumar(numero1: 10, numero2: 12)

func sumarNumero1(_ numero1: Int, conNumero2 numero2: Int) -> Int{
    return numero1 + numero2
}

sumarNumero1(10, conNumero2: 12)

func sumar(_ numero1: Int, _ numero2: Int) -> Int {
    return numero1 + numero2
}

sumar(10, 12)

var valores1 : [Any] = [Any]()
var valores2 : [Any] = []

var valores3 = [Any]()
var arrayNombres : [String] = ["Kenyi", "Rodrigo", "Eduardo"]

//for nombreIterador in coleccionDatos { codigo }

for i in 0..<10{
    print("numero \(i)")
}

for nombre in arrayNombres {
    print(nombre)
}

print("******************************************")
for algunaCosa in arrayNombres.enumerated() {
    print("\(algunaCosa.offset) - \(algunaCosa.element)")
}

print("******************************************")
for (index, nombre) in arrayNombres.enumerated() {
    print("\(index) - \(nombre)")
}

var dicAlumno : [String: Any] = ["nombre"   : "Kenyi",
                                 "dni"      : 87654321]

dicAlumno["apellido"] = "Rodriguez"

print("******************************************")
for (llave, valor) in dicAlumno {
    print("\(llave) : \(valor)")
}

print("******************************************")
print(dicAlumno["nombre"])

typealias DiccionarioAlumno = [String: Any]

var arrayAlumnos = [DiccionarioAlumno]()
