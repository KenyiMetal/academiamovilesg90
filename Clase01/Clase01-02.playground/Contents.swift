import UIKit

var nombre: String?
nombre = "kenyi"
print(nombre ?? "No tiene nombre")

//if let
//guard let

func validarConIfLet() {
    if let nombreSeguro = nombre {
        print("Existe el nombre y este es: \(nombreSeguro)")
    }else{
        print("No existe el nombre")
    }
}

func validarConGuardLet(){
    
    guard let nombreSeguro = nombre else {
        print("No existe el nombre")
        print("Ingresa un valor")
        print("Me voy a detener")
        return
    }
    
    print("Existe el nombre y este es: \(nombreSeguro)")
}

let diccionario: [String: Any] = ["nombre" : "kenyi"]
print(diccionario["nombre"] ?? "")

typealias OperacionesBasicas = (suma: Double, resta: Double, producto: Double, div: Double)

func hacerOperacionesBasicasConNumero1(_ numero1: Double, yNumero2 numero2: Double) -> OperacionesBasicas {
    
    let suma        = numero1 + numero2
    let resta       = numero1 - numero2
    let producto    = numero1 * numero2
    let div         = numero1 / numero2
    
    let operaciones : OperacionesBasicas = (suma, resta, producto, div)
    return operaciones
}

let operaciones = hacerOperacionesBasicasConNumero1(21, yNumero2: 10)
print("La suma es: \(operaciones.suma)")
print("La resta es: \(operaciones.resta)")
print("El producto es: \(operaciones.producto)")
print("La división es: \(operaciones.div)")





//Closures

typealias Sumatoria = (_ suma: Int, _ resta: Int) -> Void
func sumarNumero1(_ numero1: Int, conNumero2 numero2: Int, conResultado resultado: Sumatoria) {
    
    let suma    = numero1 + numero2
    let resta   = numero1 - numero2
    
    resultado(suma, resta)
}

print("Ejecuta1")
print("Ejecuta2")

sumarNumero1(10, conNumero2: 7) { (suma, resta) in
    
    print("Ejecuta en algún momento")
    print("Suma: \(suma)")
    print("Resta: \(resta)")
}

print("Ejecuta3")
print("Ejecuta4")




typealias NombreCompleto = (_ nombreCompleto: String) -> Void
typealias Error = (_ mensajeError: String) -> Void

var nombreAlumno: String?
var appellidoAlumno: String?

func validarDatosAlumno(nombre: String?, apellido: String?, correcto: NombreCompleto, error: Error) {
    
    guard let nombreSeguro = nombre else {
        error("Ingresa un nombre")
        return
    }
    
    guard let apellidoSeguro = apellido else {
        error("Ingresa un apellido")
        return
    }
    
    let nombreCompleto = "\(nombreSeguro) \(apellidoSeguro)"
    correcto(nombreCompleto)
}

print("**************************")
validarDatosAlumno(nombre: "kenyi", apellido: "Rodriguez", correcto: { (nombreCompleto) in
    print("El nombre es: \(nombreCompleto)")
}) { (mensajeError) in
    print(mensajeError)
}



enum DocumentosIdentidad: Int {
    case ninguno    = 0
    case dni        = 1
    case ce         = 2
    case pass       = 3
    
    var validaciones: (maximo: Int, tipoTeclado: UIKeyboardType) {
        switch self {
        case .dni:
            return (8, .numberPad)
        case .ce:
            return (8, .alphabet)
        case .pass:
            return (8, .alphabet)
        default:
            return (8, .numberPad)
        }
    }
}


let documento: DocumentosIdentidad = .ninguno
let creacionNumerica = DocumentosIdentidad.init(rawValue: 10) ?? .ninguno

let txt = UITextField()
txt.keyboardType = creacionNumerica.validaciones.tipoTeclado
