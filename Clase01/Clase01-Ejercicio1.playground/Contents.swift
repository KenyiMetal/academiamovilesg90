import UIKit

typealias DicAlumno = [String: Any]

func agregarAlumnoConNombre(_ nombre: String, apellido: String) -> DicAlumno {
    
    let dicAlumno : DicAlumno = ["nombre"   : nombre,
                                 "apellido" : apellido]
    
    return dicAlumno
}

var arrayAlumnos = [DicAlumno]()
arrayAlumnos.append(agregarAlumnoConNombre("Kenyi", apellido: "Rodriguez"))
arrayAlumnos.append(agregarAlumnoConNombre("Eduardo", apellido: "Medina"))

func imprimirAlumno(_ alumno: DicAlumno) {
    
    print("******************************")
    for (key, value) in alumno {
        print("\(key) : \(value)")
    }
}

for alumno in arrayAlumnos {
    imprimirAlumno(alumno)
}
